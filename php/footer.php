<div class="footer">
    <div class="footer-info">
        <h3>GET IN CONTACT</h3>
        <p><strong>Vaucluse NSW 2030</strong></p>
        <p><strong>Ph.</strong> 0416241930</p>
        <p><strong>E.</strong> the boutiquebonsai@gmail.com</p>
        <ul>
            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
        </ul>
        <h3>SIGN UP TO OUR NEWSLETTER</h3>
        <input type="email" class="email" placeholder="email"/>
        <button><a href="#">SIGN UP</a></button>
    </div>

    <div class="footer-text">
        <i class="fa fa-quote-left" style="font-size:24px"></i>
        <p>consectetur adipiscintg elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim laudantium, totamm rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae</p>
        <h3>John Smith</h3>
        <ul>
            <li><i class="fa fa-circle" style="font-size:8px"></i></li>
            <li><i class="fa fa-circle" style="font-size:8px"></i></li>
            <li><i class="fa fa-circle" style="font-size:8px"></i></li>
            <li><i class="fa fa-circle" style="font-size:8px"></i></li>
        </ul>
    </div>

    <div class="footer-icons">
        <h3><a href="#">SIGN UP</a></h3>
        <ul>
            <li><a href="#"><i class="fa fa-facebook" style="color: #FFFFFF"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram" style="color: #FFFFFF"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube" style="color: #FFFFFF"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin" style="color: #FFFFFF"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" style="color: #FFFFFF"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest-p" style="color: #FFFFFF"></i></a></li>
        </ul>
    </div>
</div>

<script>
    (function ($) {
        $('.menu').on('click', function () {
            console.log('clicked')
            $('.nav').addClass('open');
        })
        $('.close').on('click', function (e) {
            e.preventDefault();
            console.log('clicked')
            $('.nav').removeClass('open');
        })
    })(jQuery)
</script>
</body>
</html>