<?php include_once "header.php";?>
<div class="news-archive">
    <h1>NEWS AND MEDIA</h1>
    <div class="heading">
        <h3><a href="news.php">LATEST</a></h3>
        <h3><a href="news-archive.php">ARCHIVE</a></h3>
    </div>
    <div class="wrapper">
        <div class="row1 clearfix">
            <div class="archive left">
                <img src="../images/image002.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">OCT 4, 2016</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>WHO AM I..</h3>
                <button><a href="#">READ MORE</a></button>
            </div>

            <div class="archive left">
                <img src="../images/160407-DavidSegal-0017-MR.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">SEP 4, 2016</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>A CORKY TALE (ULMUS x HOLLANDICA</h3>
                <button><a href="#">READ MORE</a></button>
            </div>

            <div class="archive left">
                <img src="../images/Bonsaifocus2-1024x670.jpeg" alt=""/>
                <div class="date-container">
                    <p class="date">JUL 4, 2016</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>IN THE MEDIA WITH BONSAI FOCUS</h3>
                <button><a href="#">READ MORE</a></button>
            </div>

            <div class="archive left">
                <img src="../images/160407-1024x682.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">JUL 4, 2016</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>THE WORLD OF BONSAI</h3>
                <button><a href="#">READ MORE</a></button>
            </div>
        </div>

        <div class="row2 clearfix">
            <div class="archive left">
                <img src="../images/restyling.png" alt=""/>
                <div class="date-container">
                    <p class="date">JUN 23, 2016</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>THE TALE OF A FIELD GROWN CHINESE ELM</h3>
                <button><a href="#">READ MORE</a></button>
            </div>

            <div class="archive left">
                <img src="../images/160407-1024x682.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">JUN 23, 2016</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>THE UNIQUE SWAMP CYPRESS BONSAI</h3>
                <button><a href="#">READ MORE</a></button>
            </div>

            <div class="archive left">
                <img src="../images/160407-DavidSegal-0017-MR.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">JUN 23, 2016</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>BONSAI FAQ’S</h3>
                <button><a href="#">READ MORE</a></button>
            </div>
    </div>
</div>
<?php include_once "footer.php";?>
