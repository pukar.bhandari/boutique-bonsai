<?php include_once("header.php");?>
<div class="profile">
    <div class="wrapper">
        <div class="profile-first">
            <div class="profile-info">
            <h2>PROFILE</h2>
            <p>David Segal has been a dedicated student of Bonsai for almost 10 years. His studies have seen him under the tutelage of many of the worlds most respected and skilled Bonsai artists.</p>
            <h3>RECEIVED TUTELAGE</h3>
            <ul>
                <li>Leong Kwong – Sydney, Australia</li>
                <li>Ryan Neil – Oregon – USA</li>
                <li>Graham Potter – Great Yarmouth – UK</li>
                <li>Mark Noelanders – Belgium</li>
                <li>Robert Stevens – Indonesia</li>
                <li>Mauro Stemberger - Italy</li>
            </ul>
            </div>

            <div class="profile-img">
                <img src="../images/Japanese_White_Pine,_1625-2007.jpg" alt="">
            </div>
        </div>

        <div class="profile-second">
            <div class="profile-services">
                <h2>OUR FOUNDER</h2>
                <p>David is confident with all species of Bonsai and offers a complete range of services, including:</p>
                <ul>
                    <li>Maintenance</li>
                    <li>Initial Styling</li>
                    <li>Refinement Work</li>
                    <li>Repotting</li>
                    <li>Private Tuition</li>
                </ul>
            </div>
                <div class="services-img">
                    <img src="../images/tree-1272347_1920.jpg" alt=""/>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once("footer.php");?>



