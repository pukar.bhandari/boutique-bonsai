<?php include_once "header.php";?>
<div class="news-media">
    <h1>NEWS AND MEDIA</h1>
    <div class="heading">
        <h3><a href="news.php">LATEST</a></h3>
        <h3><a href="news-archive.php">ARCHIVE</a></h3>
    </div>
    <div class="wrapper">
        <div class="col1">
            <div class="post1">
                <img src="../images/160407-DavidSegal-0043-MR-1024x682.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">FEB 4, 2017</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>THE BIRTHPLACE OF BONSAI</h3>
                <button><a href="#">READ MORE</a></button>
            </div>

            <div class="post2">
                <img src="../images/shutterstock_179745623-1024x749.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">JAN 4, 2017</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>FINDING THE RIGHT BALANCE - SUNLIGHT & WATER</h3>
                <button><a href="#">READ MORE</a></button>
            </div>
        </div>

        <div class="col2">
            <div class="post3">
                <img src="../images/IMG_0897-1.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">FEB 17, 2017</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>PATIENCE & PERSEVERANCE</h3>
                <p>Being an impetuous person by nature, the patience and perseverance Bonsai demands was a concept that I struggled with for many years. Learning that if I wished to become involved and collaborate with ...</p>
                <button><a href="#">READ MORE</a></button>
            </div>
        </div>

        <div class="col3">
            <div class="post4">
                <img src="../images/blogpic-1024x683.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">DEC 4, 2016</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>FERTILISING BONSAI</h3>
                <button><a href="#">READ MORE</a></button>
            </div>

            <div class="post5">
                <img src="../images/160407-DavidSegal-0025-LR.jpg" alt=""/>
                <div class="date-container">
                    <p class="date">NOV 4, 2016</p>
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
                <h3>5 LESSONS: GROWING YOUR BONSAI</h3>
                <button><a href="#">READ MORE</a></button>
            </div>
        </div>
        </div>
    </div>

<div class="video-container">
    <div class="wrapper">
        <div class="video">
            <h3>OUR YOUTUBE CHANNEL</h3>
            <img class="img" src="../images/BonsaiTridentMaple.jpg" alt=""/>
            <div class="video-overlay"><i class="fa fa-play"></i><br>
                Play
            </div>
        </div>
    </div>
</div>
<?php include_once "footer.php";?>

