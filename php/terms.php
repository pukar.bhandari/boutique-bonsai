<?php include_once "header.php";?>
<div class="terms">
    <div class="wrapper">
        <div class="terms-text">
            <h2>TERMS AND CONDITIONS</h2>
            <p>The use of this website is governed by the policies, terms and conditions set forth below. Your use of this website and orders you place indicates your acceptance of these terms and conditions. Boutique Bonsai reserves the right to make changes to this site and these terms and conditions without notice.</p>
            <h3>PRICING</h3>
            <p>Prices displayed on this website are inclusive of GST and are exclusive of delivery charges. Prices are in Australian Dollars ($AUD) and subject to change without notice.</p>
            <h3>IMAGES</h3>
            <p>The Site attempts to display product images shown on the site as accurately as possible. However, we cannot guarantee that the colour you see matches the product colour, as the display of the colour depends, in part, upon the monitor you are using. The images of our live trees are a guide only, as they are living organism that continues to grow and change in shape over time.</p>
            <h3>PROCESSING YOUR ORDER</h3>
            <p>All items in your order that are in stock will be processed and dispatched from our nursery within 5 to 7 working days. If any items in your order are temporarily or permanently out of stock, we will contact you to arrange an exchange, credit note or refund for those items.</p>
            <h3>ORDER ACCEPTANCE</h3>
            <p>Your receipt of an e-mail order confirmation does not signify our acceptance of your order, nor does it confirm our offer to sell any products to you. Boutique Bonsai reserves the right to accept or decline your order for any reason at any time. In some cases, additional verification is needed before your order can be accepted. If additional information is required, we will attempt to contact you via e-mail or phone using the information you provided with your order. Shipping and delivery times given on our site will not take effect until your order is approved. If we are unsuccessful in our attempts to contact you, we have the right to refuse or cancel your order regardless of whether or not your order has been confirmed with confirmation e-mail.</p>
            <h3>LIABILITY</h3>
            <p>Boutique Bonsai shall not be liable for any special or consequential damages and losses that result from the use of, or the inability to use, the materials on this site or any of the websites linked or the performance of the products.</p>
            <h3>DELIVERY</h3>
            <p>The delivery times provided by Boutique Bonsai are estimates only. Boutique Bonsai will not be held accountable for late deliveries or loss or damages relating to deliveries.</p>
            <p>We CANNOT send PLANTS to Western Australia, Northern Territory and Tasmania due to quarantine restrictions. However, if you have a large enough volume order to cover the quarantine costs, please contact us.</p>
            <p>Please note we reserve the right to hold any order containing live plants if we think the weather is not suitable to send immediately. We are not held responsible for delivery delays due to public holidays. We will always contact you in such a situation, or you can notify us of any concerns.</p>
            <h3>ACCEPTANCE OF THE GOODS</h3>
            <p>The buyer is responsible for inspecting the goods for fault and notifying us within 5 working days of receiving the goods should there be a fault.</p>
            <h3>CHANGE OF ADDRESS</h3>
            <p>The shipping address for your order is shown in the checkout and on your order confirmation and shipping confirmation emails. In the event that an incorrect address was entered, address changes can only be made until the order begins processing. Boutique Bonsai cannot accommodate all address change requests. Boutique Bonsai takes no responsibility for orders shipped to an incorrect or invalid address and is not liable for any loss or damage associated.</p>
            <p>For refunds and exchanges please refer to our Refund and Exchange policy. Any items returned outside the timeframe or items returned damaged or soiled (unless faulty) will be the responsibility of the customer to retrieve and will not be given a refund.</p>
            <h3>REFUNDS AND EXCHANGES – LIVE PLANTS</h3>
            <p>Due to the perishable nature of plants there will be no refunds and exchanges offered. Please understand that we cannot be responsible for extreme weather nor for the health of a plant once it is out of our care. We do handle and pack them to the best of our ability; however, Boutique Bonsai will not be held accountable for late deliveries or loss or damages relating to live plants deliveries, due to their perishable nature.</p>
            <h3>COPYRIGHT AND TRADEMARK NOTICE</h3>
            <p>This site is owned and operated by Boutique Bonsai. Unless otherwise specified, all materials appearing on this site, including the text, site design, logos, graphics, icons, and images, as well as the selection, assembly and arrangement, are the sole property of Boutique Bonsai.</p>
            <p>You may use the content of this site only for the purpose of shopping on this site or placing an order on this site and for no other purpose.</p>
            <p>No materials from this site may be copied, reproduced, modified, republished, uploaded, posted, transmitted, or distributed in any form or by any means without Boutique Bonsai providing prior written permission.</p>
            <p>Any unauthorized use of the materials appearing on this site may violate copyright, trademark and other applicable laws and could result in criminal or civil penalties.</p>
            <h3>TYPOGRAPHICAL ERRORS</h3>
            <p>In the event a product is listed at an incorrect price or with incorrect information due to typographical error or error in product information received from our suppliers, Boutique Bonsai shall have the right to refuse or cancel any orders placed for the product listed at the incorrect price whether or not the order has been confirmed or you have been charged.</p>
            <p>If you have been charged for the purchase and your order is cancelled, Boutique Bonsai shall immediately issue a refund for your order using the original payment method.</p>
            <h3>USE OF SITE</h3>
            <p>Harassment in any manner or form on the site, including via e-mail, chat, or by use of obscene or abusive language, is strictly forbidden. Impersonation of others, including Boutique Bonsai or other licensed employee, host, or representative, as well as other members or visitors on the site is prohibited. You may not upload to, distribute, or otherwise publish through the site any content which is libellous, defamatory, obscene, threatening, invasive of privacy or publicity rights, abusive, illegal, or otherwise objectionable which may constitute or encourage a criminal offence, violate the rights of any party or which may otherwise give rise to liability or violate any law. You may not upload commercial content on the site or use the site to solicit others to join or become members of any other commercial online service or other organisation.</p>
            <h3>PARTICIPATION DISCLAIMER</h3>
            <p>Boutique Bonsai does not and cannot review all communications and materials posted to or created by users accessing the site, and are not in any manner responsible for the content of these communications and materials. You acknowledge that by providing you with the ability to view and distribute user-generated content on the site, Boutique Bonsai is merely acting as a passive conduit for such distribution and is not undertaking any obligation or liability relating to any contents or activities on the site. However, Boutique Bonsai reserves the right to block or remove communications or materials that it determines to be (a) abusive, defamatory, or obscene, (b) fraudulent, deceptive, or misleading, (c) in violation of a copyright, trademark or; other intellectual property right of another or (d) offensive or otherwise unacceptable to Boutique Bonsai in its sole discretion.</p>
            <h3>THIRD-PARTY LINKS</h3>
            <p>In an attempt to provide increased value to our visitors, Boutique Bonsai may link to sites operated by third parties. However, even if the third party is affiliated with Boutique Bonsai, Boutique Bonsai has no control over these linked sites, all of which have separate privacy and data collection practices, independent of Boutique Bonsai. These linked sites are only for your convenience and therefore you access them at your own risk. Nonetheless, Boutique Bonsai seeks to protect the integrity of its web site and the links placed upon it and therefore requests any feedback on not only its own site, but for sites it links to as well (including if a specific link does not work).</p>
        </div>

        <div class="recent-posts">
            <h2>RECENT POSTS</h2>
            <div class="post">
                <div class="post-img">
                    <img src="../images/160407-DavidSegal-0271-MR-500x300.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>Patience & Perseverance</p>
                    <p class="date">FEB 17, 2017</p>
                </div>
            </div>

            <div class="post">
                <div class="post-img">
                    <img src="../images/160407-DavidSegal-0043-MR-1024x682.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>The birthplace of Bonsai</p>
                    <p class="date">FEB 4, 2017</p>
                </div>
            </div>

            <div class="post">
                <div class="post-img">
                    <img src="../images/shutterstock_179745623-1024x749.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>Finding the right balance - sunlight & water</p>
                    <p class="date">JAN 4, 2017</p>
                </div>
            </div>

            <div class="post">
                <div class="post-img">
                    <img src="../images/blogpic-1024x683.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>Fertlising Bonsai</p>
                    <p class="date">DEC 4, 2016</p>
                </div>
            </div>

            <div class="post">
                <div class="post-img">
                    <img src="../images/160407-DavidSegal-0025-MR-1-500x350.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>5 Lessons: Growing Your Bonsai</p>
                    <p class="date">NOV 4, 2016</p>
                </div>
            </div>

            <div class="search">
                <h3>SEARCH</h3>
                <input type="search" id="search" placeholder="Search..."/><i class="fa fa-search"></i>
            </div>
        </div>
    </div>
</div>
<?php include_once "footer.php";?>
