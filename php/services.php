<?php include_once("header.php");?>
<div class="services">
    <div class="wrapper">
        <div class="services-first">
            <div class="services-first-text">
                <h2>SERVICES</h2>
                <p>Boutique Bonsai offers a full range of Bonsai services, including :</p>
                <p>Bonsai trees for purchase of various species, sizes and styles – see our Gallery for current available trees and prices</p>
                <p>Special Projects – We have the ability to source and create custom Bonsai Trees to suit your specific requirements. <a href="enquire.php"><strong>Enquire now</strong></a> for more information.</p>
                <ul>
                    <li>Pruning / Shaping / Wiring</li>
                    <li>Repotting</li>
                    <li>Restyling</li>
                    <li>Private tuition</li>
                </ul>
            </div>
            <div class="services-first-img">
                <img src="../images/160407-DavidSegal-0025-MR-1-500x350.jpg" alt=""/>
            </div>
        </div>

        <div class="services-second">
            <div class="services-first-text">
                <h2>PRICING</h2>
                <p>Prices are based on a <strong>flat hourly rate of $100.</strong> Larger projects and extended tuition can be negotiated by way of a flat fee, please enquire to discuss further.</p>
                <button><a href="enquire.php">ENQUIRE NOW</a></button>
            </div>
            <div class="services-second-img">
                <img src="../images/160407-DavidSegal-0128-MR-500x350.jpg" alt=""/>
            </div>
        </div>

        <div class="services-third">
            <div class="services-first-text">
                <h2>YOUR BONSAI IS A COMPLEX ORGANISM</h2>
                <p>It requires a sound knowledge of basic horticultural concepts. Bonsai’s are like every other tree in nature and require all of the same elements such as sun, water and nutrition. What makes Bonsai more challenging is that you are providing a miniature tree with all that it requires within a confined space.</p>
                <button><a href="enquire.php">ENQUIRE NOW</a></button>
                <h4>Pruning/Refinement:</h4>
                <p>As your Bonsai grows and develops it will require regular pruning to maintain it’s shape and style.</p>
                <h4>Initial Styling:</h4>
                <p>Bonsai trees do not occur naturally in nature, they are created through careful branch selection and with the use of wire and specialised shaping techniques. Boutique Bonsai can create brand new Bonsai from raw untouched plant material.</p>
            </div>
            <div class="services-first-img">
                <img src="../images/160407-DavidSegal-0176-MR-500x350.jpg" alt=""/>
            </div>
        </div>

        <div class="services-fourth">
            <div class="services-first-text">
                <h4>Restyling:</h4>
                <p>During the life cycle of a Bonsai it may take on different shapes and sizes as it continues to grow or your aesthetic changes. Taking an existing Bonsai and transforming it into something entirely different requires a specific skill set and many years of experience and practice.</p>
                <h4>Repotting</h4>
                <p>As the foliage on a Bonsai grows and develops, so too does it’s root system. Depending on the species, age and level of health of a Bonsai, it will require root pruning so that it can maintain the optimum level of health and beauty. The timing on this can vary from as little as1 year to as many as 5 or 10 years.</p>
            </div>
            <div class="services-second-img">
                <img src="../images/160407-DavidSegal-0140-MR-500x350.jpg" alt=""/>
            </div>
        </div>
    </div>
</div>
<?php include_once("footer.php");?>