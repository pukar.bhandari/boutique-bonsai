<?php include_once "header.php";?>
<div class="images-gallery">
    <h1>GALLERY</h1>
    <div class="wrapper">
        <div class="col-1">
            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/1.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/5.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/9.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/12.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/16.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>
        </div>

        <div class="col-2">
            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/2.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/6.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/10.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/13.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/17.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>
        </div>

        <div class="col-3">
            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/3.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/7.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/11.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/14.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/18.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>
        </div>

        <div class="col-4">
            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/4.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/8.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/15.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/19.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>

            <div class="images-gallery-img">
                <img class="img" src="../images/gallery/20.jpg" alt=""/>
                <div class="img-overlay">BALD CYPRESS(TAXODIUM DISTICHUM)</div>
            </div>
        </div>
    </div>
</div>

<?php include_once "footer.php";?>
