<?php include_once "header.php";?>
<div class="terms">
    <div class="wrapper">
        <div class="terms-text">
            <h2>PRIVACY POLICY</h2>
            <p>Boutique Bonsai is committed to providing the very best customer service possible. We only collect limited information to process your order properly and securely, and will NEVER sell or distribute your information. We may use your information to keep you informed of our special offers regarding our products, that is only if you have subscribed to our newsletter. You may unsubscribe from our newsletter anytime.</p>
        </div>

        <div class="recent-posts">
            <h2>RECENT POSTS</h2>
            <div class="post">
                <div class="post-img">
                    <img src="../images/160407-DavidSegal-0271-MR-500x300.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>Patience & Perseverance</p>
                    <p class="date">FEB 17, 2017</p>
                </div>
            </div>

            <div class="post">
                <div class="post-img">
                    <img src="../images/160407-DavidSegal-0043-MR-1024x682.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>The birthplace of Bonsai</p>
                    <p class="date">FEB 4, 2017</p>
                </div>
            </div>

            <div class="post">
                <div class="post-img">
                    <img src="../images/shutterstock_179745623-1024x749.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>Finding the right balance - sunlight & water</p>
                    <p class="date">JAN 4, 2017</p>
                </div>
            </div>

            <div class="post">
                <div class="post-img">
                    <img src="../images/blogpic-1024x683.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>Fertlising Bonsai</p>
                    <p class="date">DEC 4, 2016</p>
                </div>
            </div>

            <div class="post">
                <div class="post-img">
                    <img src="../images/160407-DavidSegal-0025-MR-1-500x350.jpg" alt=""/>
                </div>
                <div class="post-text">
                    <p>5 Lessons: Growing Your Bonsai</p>
                    <p class="date">NOV 4, 2016</p>
                </div>
            </div>

            <div class="search">
                <h3>SEARCH</h3>
                <input type="search" id="search" placeholder="Search..."/><i class="fa fa-search"></i>
            </div>
        </div>
    </div>
    <div class="privacy_policy"></div>
</div>
<?php include_once "footer.php";?>
