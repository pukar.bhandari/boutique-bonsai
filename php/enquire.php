<?php include_once("header.php");?>
    <div class="wrapper">
        <div class="map">
            <h2>CONTACT US</h2>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6627.480477857459!2d151.281446!3d-33.844809!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x326aa6a1e60423f3!2sVaucluse%20Yacht%20Club!5e0!3m2!1sen!2sus!4v1568629275352!5m2!1sen!2sus" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
        <div class="wrapper-second">
            <div class="form">
                <div class="contact">
                    <h3>CONTACT INFORMATION</h3>
                    <p><strong>Vaucluse NSW 2030</strong></p>
                    <p><strong>Ph.</strong> 0416241930</p>
                    <p><strong>E.</strong> the boutiquebonsai@gmail.com</p>
                </div>

                <div class="form-input">
                    <h2>GET IN TOUCH</h2>
                    <form id="survey-form">
                        <input id="name" type="text" placeholder="Name"/>
                        <input id="email" type="email" placeholder="Email"/>
                        <input id="subject" type="text" placeholder="Subject"/>
                        <textarea rows="5" cols="50" name="comment" form="survey-form" placeholder="Message"></textarea>
                        <img src="../images/recaptcha.png" alt=""/>
                        <button><a href="#">SEND</a></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include_once("footer.php");?>