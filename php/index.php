<?php
    include_once("header.php");
?>
<div class="main">
    <div class="wrapper">
        <div class="first-image">
            <img src="../images/slide1.jpg" alt=""/>
        </div>

        <div class="wrapper">
            <div class="shop">
                <h2>SHOP</h2>
                <div class="shop-img">
                    <div class="start"><img src="../images/Bonsai_tree.jpg" alt=""/></div>
                    <div class="middle"><img src="../images/Bonsai_trees.jpg" alt=""/></div>
                    <div class="end"><img src="../images/29840763790_22fc732556_k.jpg" alt=""/></div>
                </div>
            </div>

            <div class="third">
                <div class="lt-work">
                    <h2>LATEST WORK</h2>
                    <h3>THIS IS THE SPOT FOR LATEST WORK</h3>
                    <p>Consectetur adipiscintg elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim laudantium, totamm rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem</p>
                    <button><a href="#">READ MORE</a></button>
                    <div class="back">
                        <i class="fa fa-angle-left"></i>
                        <i class="fa fa-angle-right"></i>
                    </div>
                </div>

                <div class="lt-works-img">
                    <img src="../images/Japanese_White_Pine,_1625-2007.jpg" alt=""/>
                </div>
            </div>

            <div class="fourth">
                <div class="news-img">
                    <img src="../images/IMG_0897-1.jpg" alt=""/>
                </div>

                <div class="news">
                    <h2>NEWS AND MEDIA</h2>
                    <h3>THIS IS THE TITLE OF THE STORY</h3>
                    <p>Consectetur adipiscintg elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim laudantium, totamm rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem</p>
                    <button><a href="#">READ MORE</a></button>
                    <div class="back">
                        <i class="fa fa-angle-left"></i>
                        <i class="fa fa-angle-right"></i>
                    </div>
                </div>
            </div>

            <div class="video">
                <img class="img" src="../images/BonsaiTridentMaple.jpg" alt=""/>
                <div class="video-overlay"><i class="fa fa-play"></i><br>
                    Play
                </div>
            </div>

            <div class="gallery">
                <div class="hashtag">
                    <h3>GROW WITH US</h3>
                    <p>#boutiquebonsai</p>
                </div>

                <div class="gallery-img">
                    <img class="img" src="../images/Bonsai_tree.jpg" alt="">
                    <div class="video-overlay"><i class="fa fa-heart"></i></div>
                </div>

                <div class="gallery-img">
                    <img class="img" src="../images/Bonsai_trees.jpg" alt="">
                    <div class="video-overlay"><i class="fa fa-heart"></i></div>
                </div>

                <div class="gallery-img">
                    <img class="img" src="../images/29840763790_22fc732556_k.jpg" alt="">
                    <div class="video-overlay"><i class="fa fa-heart"></i></div>
                </div>

                <div class="gallery-img">
                    <img class="img" src="../images/Bonsai_tree.jpg" alt="">
                    <div class="video-overlay"><i class="fa fa-heart"></i></div>
                </div>

                <div class="gallery-img">
                    <img class="img" src="../images/Bonsai_trees.jpg" alt="">
                    <div class="video-overlay"><i class="fa fa-heart"></i></div>
                </div>

                <div class="gallery-img">
                    <img class="img" src="../images/29840763790_22fc732556_k.jpg" alt="">
                    <div class="video-overlay"><i class="fa fa-heart"></i></div>
                </div>

                <div class="button">
                    <button><a href="#">SEE MORE</a></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    include_once("footer.php");
?>