<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Assignment 5</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="../js/jquery.js"></script>
</head>
<body>

<div class="nav">
    <ul class="nav-first">
        <li class="close"><a href=""#">CLOSE</a></li>
        <li><a href="index.php">HOME</a></li>
        <li><a href="services.php">SERVICES</a></li>
        <li><a href="gallery.php">GALLERY</a></li>
        <li><a href="news.php">NEWS AND MEDIA</a></li>
        <li><a href="about.php">ABOUT</a></li>
        <li><a href="enquire.php">CONTACT US</a></li>
    </ul>

    <ul class="nav-second">
        <li><a href="terms.php">Terms and Conditions</a></li>
        <li><a href="privacy-policy.php">Privacy Policy</a></li>
        <li><a href="news-archive.php">News and Media Archive</a></li>
    </ul>
</div>
<div class="header">
    <div class="menu"><a class="#">
            <p>
                <i class="fa fa-bars"></i>MENU
            </p>
        </a>
    </div>

    <div class="logo">
        <a href="index.php"><img src="../images/logo.png" alt="logo"/></a>
    </div>

    <div class="enquire">
        <a href="enquire.php">ENQUIRE NOW</a>
    </div>

</div>

<div class="download">
    <button><a href="#">DOWNLOAD YOUR BONSAI HELP GUIDE</a></button>
</div>

<div class="icons">
    <ul>
        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
    </ul>
</div>